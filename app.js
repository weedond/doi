var http    		= require('http'),
    sys     		= require('sys'),
    express 		= require('express'),
    io 				= require('socket.io');

    THREE			= require('three');
    players			= require('./modules/players.js');
    Lasers			= require('./modules/lasers.js');
    lasers 			= new Lasers();
    date 			= new Date();

   	// Game Imports
   	

var port = process.env.PORT || 5000;

var app = express();
app.use(express.static(__dirname + '/public'));

var socket = io.listen(http.createServer(app).listen(port, '0.0.0.0'));
sys.log('Started server on http://localhost:' + port + '/');


socket.sockets.on('connection', function(client) {
	
	var clock  = new THREE.Clock();
	var user = players.addPlayer('David');


	client.emit('init', {
		id : user.id,
		name : user.name,
		players : players.getPacket(),
		time : date.getTime()
	});

	client.broadcast.emit('newPlayer', {
		id: user.id,
		name: user.name
	});

	client.on('disconnect', function() {
		players.removePlayer(user.id);
		socket.sockets.emit('removePlayer', user.id);
	});

	client.on('input', function(input) {
		// Fake some lag
		var delta = clock.getDelta();
		user.update({
			delta : delta,
			input : input.input,
			seq   : input.seq,
		});
		lasers.update(delta, players);
	});



	var lag = 0;

	var ping = function() {
		var d = new Date();
		var time = d.getTime();
		client.emit('ping', { latency: lag, time: time });
		setTimeout(ping, 100);
	};
	ping();
	client.on('pong', function(oldTime) {
		var d = new Date();
		var newTime = d.getTime();
		lag = (newTime - oldTime) / 2;
	sys.log(lag);
	});

});


var updateLoop = function() {
	var d = new Date();
	var t = d.getTime();
	socket.sockets.emit('update', {
		players : players.getPacket(t),
		lasers  : lasers.getPacket(),
		time : t
	});
	setTimeout(updateLoop, 15);
}
updateLoop();