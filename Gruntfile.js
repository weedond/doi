
module.exports = function(grunt) {

	grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    browserify: {
    	 client: {
	        src: ['client/**/*.js'],
	        dest: 'public/client.js',
      	}
    },

    watch: {
      client: {
        files: ['client/**/*.js'],
        tasks: ['jshint', 'browserify:client']
      }
    },

    jshint: {
      files: ['Gruntfile.js', 'client/**/*.js', '!client/vendor/**/*.*'],
      options: {
        // options here to override JSHint defaults
        "browserify": true,
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true,
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');

};